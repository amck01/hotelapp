<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.amck01.hotelapp.HotelRoom" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Example Hotel App</title>
</head>
<body>
<b>Example Hotel App</b>
<br>
<%= new java.util.Date() + "<br>" %>

<%
	HotelRoom hotelRoom1 = new HotelRoom(101, 2, true);
	out.println(hotelRoom1.getRoomNumber() + "<br>");
	out.println(hotelRoom1.getCapacity() + "<br>");
	out.println(hotelRoom1.isHasOceanView() + "<br>");
%>
</body>
</html>