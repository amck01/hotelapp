package com.amck01.hotelapp;

public class HotelRoom {
	private int roomNumber;
	private int capacity;
	private boolean hasOceanView;
	
	public HotelRoom(int roomNumber, int capacity, boolean hasOceanView) {
		super();
		this.roomNumber = roomNumber;
		this.capacity = capacity;
		this.hasOceanView = hasOceanView;
	}
	
	public int getRoomNumber() {
		return roomNumber;
	}
	
	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public boolean isHasOceanView() {
		return hasOceanView;
	}
	
	public void setHasOceanView(boolean hasOceanView) {
		this.hasOceanView = hasOceanView;
	}
	
	
}
